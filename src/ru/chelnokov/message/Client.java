package ru.chelnokov.message;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

@SuppressWarnings("InfiniteLoopStatement")
public class Client {
    public static void main(String[] args)  {
        Scanner reader = new Scanner(System.in);
        try(Socket clientSocket = new Socket("localhost", 8081)){
            System.out.println("Он ждёт сообщения, обязательно напиши!!!");

            ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
            String message = reader.nextLine();
            outputStream.writeObject(message);
            System.out.println("Вы написали: " + message);
            outputStream.flush();

            ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
            while (true) {
                String answer = (String) inputStream.readObject();
                System.out.println("...и он повторил: " + answer);

                System.out.println("Напиши ему ещё!");
                message = reader.nextLine();
                outputStream.writeObject(message);
                System.out.println("Вы написали: " + message);
                outputStream.flush();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

