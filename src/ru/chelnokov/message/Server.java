package ru.chelnokov.message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

@SuppressWarnings("InfiniteLoopStatement")
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8081);
        System.out.println("Жду, когда мне напишут...");
        try (Socket clientSocket = serverSocket.accept();
             ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
             ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream())) {

            String message;
            while (true) {
                message = (String)inputStream.readObject();
                System.out.println("Мне написали: " + message);
                if (message.contains("дурак")) {
                    System.out.println("\"Сам он дурак\", - подумал я, и больше с ним не разговариваю!");
                    break;
                }
                outputStream.writeObject(message);
                outputStream.flush();
                System.out.println("...и я повторил " + message);
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
