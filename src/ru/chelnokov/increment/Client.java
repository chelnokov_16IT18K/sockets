package ru.chelnokov.increment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Класс клиента для приложения "инкремент"
 * от клиента поступает число, которое сервер будет инкрементить до 10
 *
 * @author Chelnokov E.I. 16it18k
 */
public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            int response = 1;
            outputStream.write(response);
            System.out.println("отправлено серверу: " + response);

            while ((response = inputStream.read()) != -1) {
                System.out.println("прислал сервер: " + response);
                if (response >= 10) {
                    break;
                }
                outputStream.write(response);
                System.out.println("отправлено серверу: " + response);
                outputStream.flush();
                response++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}